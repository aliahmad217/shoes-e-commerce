﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite.admin
{
    public partial class UpdateProducts : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pid.Text = Request.QueryString["id"];
                pname.Text = Request.QueryString["name"];
                pprice.Text = Request.QueryString["price"];
               
            }
          
            


        }

        void update_product()
        {

            string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);

            
                if (pimg.HasFile)
                {


                    pimg.PostedFile.SaveAs(Server.MapPath("~/images/") + pimg.FileName);
                    string link = "/images/" + pimg.FileName;
                    SqlCommand cmd = new SqlCommand("UPDATE products SET name='" + pname.Text + "', price='" + pprice.Text + "', img='" + link + "' WHERE id='" + pid.Text + "'", con);
                    con.Open();
                    //cmd.Parameters.AddWithValue("@pname", email_e.Text.Trim());
                    //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                    //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                    cmd.ExecuteNonQuery();
                    con.Close();
                    Response.Redirect("ViewProducts.aspx");
                }
                else
                {
                    msgLbl.Text = "Failed to upload. try again";
                }
            }
        void delete ()
        {
            string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);


            

               
                SqlCommand cmd = new SqlCommand("Delete from products WHERE id='" + pid.Text + "'", con);
                con.Open();
                //cmd.Parameters.AddWithValue("@pname", email_e.Text.Trim());
                //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("ViewProducts.aspx");
            }
           

        
        
        protected void update_btn_Click(object sender, EventArgs e)
        {
            update_product();
        }

        
    }
}