﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="ViewProducts.aspx.cs" Inherits="shoppingSite.admin.ViewProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     
    <div style="position: relative; width: 500px; height: 300px; margin: 0 auto"   class="container-fluid" >
        <div class="content">
        <div class="row">
            <div  class="col-xs-12">
    
                <asp:GridView style="align-content:center ;width: 1327px"   CssClass="table table-responsive" AutoGenerateColumns="False" runat="server" ID="product_gv" CellPadding="4" ForeColor="#333333" GridLines="None" DataSourceID="SqlDataSource1" DataKeyNames="id">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField Visible="false" DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False" ReadOnly="True" />
                        <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                        <asp:BoundField DataField="price" HeaderText="price" SortExpression="price"  />
                        <asp:ImageField ControlStyle-Width="120" ControlStyle-Height="80" DataImageUrlField="img" />
                        <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="id,name,price,img" DataNavigateUrlFormatString="UpdateProducts.aspx?id={0}&name={1}&price={2}&img={3}" />
                        <asp:HyperLinkField Text="Delete" DataNavigateUrlFields="id,name,price,img" DataNavigateUrlFormatString="DeleteProducts.aspx?id={0}&name={1}&price={2}&img={3}"/>
                       
                    </Columns>

                    <EditRowStyle BackColor="#999999"></EditRowStyle>

                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>

                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#E9E7E2"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#506C8C"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#FFFDF8"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#6F8DAE"></SortedDescendingHeaderStyle>
                </asp:GridView>
        </div>
                </div>
            </div>
            </div>
        
        
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" SelectCommand="SELECT * FROM [products]"></asp:SqlDataSource>

          
        
    
</asp:Content>
