﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="logoutconfirm.aspx.cs" Inherits="shoppingSite.admin.logoutconfirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div runat="server" style="position: relative; width: 500px; height: 300px; margin: 0 auto"   class="container-fluid">
        <div class="content">
        <div class="row">
            <div  class="col-lg-12">
                <asp:Image Width="60px" Height="100px"  runat="server" id="image" ImageUrl="images/fiverdp.jpg"></asp:Image>
                <asp:Label runat="server" ID="name"></asp:Label>
                <asp:TextBox Visible="false" runat="server" ID="iid"></asp:TextBox> 
                <h3>are u sure you want to logout?</h3>
                <br />
                <asp:Button CssClass="btn btn-primary"  Text="Yes" runat="server" ID="yes_logout" OnClick="yes_logout_btn"  />
                <asp:Button CssClass="btn btn-secondary" Text="No" runat="server" ID="no_logout" OnClick="no_logout_btn" />
               
                </div>
            </div>
            </div>
         </div>
</asp:Content>
