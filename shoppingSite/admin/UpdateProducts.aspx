﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.Master" AutoEventWireup="true" CodeBehind="UpdateProducts.aspx.cs" Inherits="shoppingSite.admin.UpdateProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="position:absolute;left:20%" >
        <h1>Update Product</h1>
     <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    
                    <div class="panel-body">

                         <label>Product Id</label>

                            <br>
                            <asp:TextBox ReadOnly="true"  ID="pid" runat="server" />
                        <br />
                        
                        <label>Product Name</label>

                            <br>
                            <asp:TextBox ID="pname" runat="server" />
                            <br>
                        <label>Product Price</label><br>
                            <asp:TextBox ID="pprice" runat="server" />
                            <br />
                        <label>Product Img</label><br>
                            <asp:FileUpload  ID="pimg" runat="server" />
                            <br />
                            <br />
                        <asp:Button CssClass="btn btn-primary" runat="server" ID="update_btn" OnClick="update_btn_Click" Text="Update Product" />
                           <br />
                        
                            <asp:Label runat="server" ID="msgLbl"></asp:Label>
                            
                    </div>

                </div>

            </div>

        </div>

    </section>
        </div>
</asp:Content>
