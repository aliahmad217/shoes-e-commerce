﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite.admin
{
    public partial class add_product : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        void addProduct()
        {
            msgLbl.Text = "";
                string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(CS);
             if (pimg.HasFile || pname.Text!="" || pprice.Text!="")
            {
                pimg.PostedFile.SaveAs(Server.MapPath("~/images/") + pimg.FileName);
                string link = "/images/" + pimg.FileName;
                SqlCommand cmd = new SqlCommand("Insert into Products(name,price,img) Values('" + pname.Text + "','" + pprice.Text + "','" + link + "')", con);
                con.Open();
                //cmd.Parameters.AddWithValue("@pname", email_e.Text.Trim());
                //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                //cmd.Parameters.AddWithValue("@password", password_e.Text.Trim());
                cmd.ExecuteNonQuery();
                con.Close();
                pname.Text = "";
                pprice.Text = "";
                msgLbl.ForeColor = System.Drawing.Color.BlueViolet;
                msgLbl.Text = "Product added successfully";

            }
            else
            {
                msgLbl.ForeColor = System.Drawing.Color.Red;
                msgLbl.Text = "Product not added .All fields must be filled. try again";
            }
        }
        protected void add_btn_Click(object sender, EventArgs e)
        {
            addProduct();
        }
    }
}