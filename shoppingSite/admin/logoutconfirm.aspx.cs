﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite.admin
{
    public partial class logoutconfirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                name.Text = Session["username"].ToString();
            }
            }

        protected void yes_logout_btn(object sender, EventArgs e)
        {
            Session["username"] = null;
            
            Response.Redirect("Default.aspx");

        }
        protected void no_logout_btn(object sender, EventArgs e)
        {
            Response.Redirect(Request.UrlReferrer.ToString());
        }
    }
}
