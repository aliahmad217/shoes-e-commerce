﻿using shoppingSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite
{
    public partial class cart_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                loadData();


                //if (Request.QueryString.AllKeys.Contains("id"))
                //{
                //    string id = Request.QueryString["id"];
                //}
            }
        }

        void loadData()
        {
            if (Session["cart"]!=null) {
                List<Products> prods = Session["cart"] as List<Products>;
                product_gv.DataSource = prods;
                product_gv.DataBind();
               
            }
        }

        protected void remove_btn(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            // Or your logic to generate id
            //string url = String.Format("cart_view.aspx?id={0}", id);
            Response.Redirect("cart_view.aspx ?id="+id   );

        }
    }
}