﻿using shoppingSite.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite
{
    public partial class checkout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        void addToCart( string product_id, string session_id)
        {
            string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("Insert into Cart(product_id,session_id) Values('" + product_id + "','" + session_id + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        void checkoutproduct()
        {
            string session_id = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            if(Session["cart"]!=null)
            {
                List<Products> cart = Session["cart"] as List<Products>;
                foreach (var item in cart)
                {
                    addToCart(item.id, session_id);
                }
                string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
                SqlConnection con = new SqlConnection(CS);
                con.Open();
                SqlCommand cmd = new SqlCommand("Insert into orders(session_id,customer_name,customer_phone,customer_addres) Values('" + session_id + "','" + name_box.Text + "','" + phone_box.Text + "','" + adress_box.Text + "')", con);
                cmd.ExecuteNonQuery();
                con.Close();
                name_box.Text ="";
                phone_box.Text = "";
                adress_box.Text = "";
                Response.Redirect("CheckoutConfirmed.aspx");

            }
        }
        protected void checkout_btn_Click(object sender, EventArgs e)
        {
            checkoutproduct();
        }
    }
}