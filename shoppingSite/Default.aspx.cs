﻿using shoppingSite.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace shoppingSite
{
    public partial class Default : System.Web.UI.Page
    {
        public List<Products> product_list = new List<Products>();
        static List<Products> cart_list = new List<Products>();
        protected void Page_Load(object sender, EventArgs e)
        {

            load_products();

            if(Request.QueryString.AllKeys.Contains("pro_id")&&(Request.QueryString.AllKeys.Contains("action")))
            {
                string pro_id = Request.QueryString["pro_id"];
                string action = Request.QueryString["action"];
                if(action.Equals("add"))
                {
                    addToCart(pro_id);
                }

            }
        }
        void addToCart(string product_id)
        {
            var product = product_list.Where(x => x.id == product_id);
            foreach(var data in product)
            {
                cart_list.Add(data);
                
            }
            Session["cart"] = cart_list;
        }
        void load_products()
        {
            string CS = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlCommand cmd = new SqlCommand("Select * from Products", con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    product_list.Add(new Products(reader["id"].ToString(), reader["name"].ToString(), reader["price"].ToString(), reader["img"].ToString()));
                }
            }
        }
    }
}