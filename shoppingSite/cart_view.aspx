﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web2.Master" AutoEventWireup="true" CodeBehind="cart_view.aspx.cs" Inherits="shoppingSite.cart_view" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <form  >
    <div style="position:relative ; width: 500px; height:auto; margin: 0 auto"   class="container-fluid">
        <div class="content">
        <div class="row">
            <div  class="col-lg-12">
                <asp:GridView style="align-content:center ;width: 1327px"   CssClass="table table-responsive" AutoGenerateColumns="false" runat="server" ID="product_gv" CellPadding="4" ForeColor="#333333" GridLines="None">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField  DataField="id" HeaderText="product id" Visible="true" />
                        <asp:ImageField ControlStyle-Width="120" ControlStyle-Height="80" DataImageUrlField="img" />
                        <asp:BoundField DataField="name" HeaderText="Product name" />
                        <asp:BoundField DataField="price" HeaderText="Product Price" />
                      
                        <%--<asp:HyperLinkField Text="Remove" DataNavigateUrlFields="product_id" DataNavigateUrlFormatString="RemoveCart.aspx?product_id={0}&name={1}&price={2}&img={3}" />  />--%>

                    </Columns>

                    <EditRowStyle BackColor="#999999"></EditRowStyle>

                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>

                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#E9E7E2"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#506C8C"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#FFFDF8"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#6F8DAE"></SortedDescendingHeaderStyle>
                </asp:GridView>
                <div class="text_center">
                    <a href="checkout.aspx " class="btn btn-success">Proceed to checkout</a>
                    <br />

                </div>
            </div>
            </div>
        </div>

    </div>
        </form>
    <br />
</asp:Content>
