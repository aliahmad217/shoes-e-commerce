﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web2.Master" AutoEventWireup="true" CodeBehind="checkout.aspx.cs" Inherits="shoppingSite.checkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    
    <div class="container">
        <div class="content">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Personal information required
                </div>
                <div class="panel-body">
                    <label>Name: <asp:TextBox  runat="server"  ID="name_box" CssClass="form-control"></asp:TextBox> </label>
                    <br />
                    <label>Phone: <asp:TextBox  runat="server" ID="phone_box" CssClass="form-control"></asp:TextBox> </label>
                    <br />
                    <label>Address: <asp:TextBox TextMode="MultiLine"  runat="server" ID="adress_box" CssClass="form-control"></asp:TextBox> </label>
                   <%-- <asp:Button  runat="server"  ID="checkout_btn" OnClick="checkout_btn_Click" Text="Checkout" />--%>
                </div>
                <asp:Button runat="server" ID="checkout_btn" CssClass="btn btn-primary" Text="Checkout" OnClick="checkout_btn_Click" />
                <div >
                    

                </div>
            </div>

        </div>

    </div>
        
</asp:Content>
